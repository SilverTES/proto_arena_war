#include "Game.h"

void Game::update()
{

    // Security Limit Zone
    if (hero.y-hero.rh<0)
        hero.MOVE_UP = false;

    if (hero.y+hero.rh>_screenH)
        hero.MOVE_DOWN = false;

    if (hero.x-hero.rw<0)
        hero.MOVE_LEFT = false;

    if (hero.x+hero.rw>_screenW)
        hero.MOVE_RIGHT = false;


    // Collisions test

    float zone = 6;

    p[0].x = hero.x-zone; // UP LEFT
    p[0].y = hero.y-zone;

    p[1].x = hero.x+zone; // UP RIGHT
    p[1].y = hero.y-zone;

    p[2].x = hero.x-zone; // DOWN LEFT
    p[2].y = hero.y+zone;

    p[3].x = hero.x+zone; // DOWN RIGHT
    p[3].y = hero.y+zone;


    for (unsigned i(0); i<4; i++)
    {
        int mapX = int(p[i].x/caseSize);
        int mapY = int(p[i].y/caseSize);

        // test collision les côtés
        if (level.get(mapX, mapY-1) == 1 && (i==0 || i==1) && p[i].y-2 <= mapY*caseSize )
        {
            hero.MOVE_UP = false;
        }

        if (level.get(mapX, mapY+1) == 1 && (i==2 || i==3) && p[i].y-14 >= mapY*caseSize )
        {
            hero.MOVE_DOWN = false;
        }

        if (level.get(mapX-1, mapY) == 1 && (i==0 || i==2) && p[i].x-2<= mapX*caseSize )
        {
            hero.MOVE_LEFT = false;
        }

        if (level.get(mapX+1, mapY) == 1 && (i==1 || i==3) && p[i].x-14 >= mapX*caseSize)
        {
            hero.MOVE_RIGHT = false;
        }

    }

    // test les diagonales
    {
        int mapX = hero.mapX;
        int mapY = hero.mapY;

        if (level.get(mapX-1, mapY-1) == 1 && hero.y-2 <= mapY*caseSize && hero.x-2<= mapX*caseSize)
        {
            hero.MOVE_UP = false;
            hero.MOVE_LEFT = false;
        }

        if (level.get(mapX+1, mapY-1) == 1 && hero.y-2 <= mapY*caseSize && hero.x-10 >= mapX*caseSize)
        {
            hero.MOVE_UP = false;
            hero.MOVE_RIGHT = false;
        }

        if (level.get(mapX-1, mapY+1) == 1 && hero.y-10 >= mapY*caseSize && hero.x-2<= mapX*caseSize)
        {
            hero.MOVE_DOWN = false;
            hero.MOVE_LEFT = false;
        }

        if (level.get(mapX+1, mapY+1) == 1 && hero.y-10 >= mapY*caseSize && hero.x-10 >= mapX*caseSize)
        {
            hero.MOVE_DOWN = false;
            hero.MOVE_RIGHT = false;
        }
    }


    // test possibilité de décalage !
    for (unsigned i(0); i<4; i++)
    {
        int tmapX = int(p[i].x/caseSize);
        int tmapY = int(p[i].y/caseSize);


        if (!hero.MOVE_UP && level.get(tmapX, tmapY-1) == 0 && hero.D_UP && !(hero.D_LEFT || hero.D_RIGHT) )
        {
            if (i==0)
            {
                hero.CAN_UP = true;
                hero.AUTO_LEFT = true;
            }

            if (i==1)
            {
                hero.CAN_UP = true;
                hero.AUTO_RIGHT = true;
            }
        }

        if (!hero.MOVE_DOWN && level.get(tmapX, tmapY+1) == 0 && hero.D_DOWN && !(hero.D_LEFT || hero.D_RIGHT) )
        {
            if (i==2)
            {
                hero.CAN_DOWN = true;
                hero.AUTO_LEFT = true;
            }

            if (i==3)
            {
                hero.CAN_DOWN = true;
                hero.AUTO_RIGHT = true;
            }
        }

        if (!hero.MOVE_LEFT && level.get(tmapX-1, tmapY) == 0 && hero.D_LEFT && !(hero.D_UP || hero.D_DOWN) )
        {
            if (i==0)
            {
                hero.CAN_LEFT = true;
                hero.AUTO_UP = true;
            }

            if (i==2)
            {
                hero.CAN_LEFT = true;
                hero.AUTO_DOWN = true;
            }
        }

        if (!hero.MOVE_RIGHT && level.get(tmapX+1, tmapY) == 0 && hero.D_RIGHT && !(hero.D_UP || hero.D_DOWN) )
        {
            if (i==1)
            {
                hero.CAN_RIGHT = true;
                hero.AUTO_UP = true;
            }

            if (i==3)
            {
                hero.CAN_RIGHT = true;
                hero.AUTO_DOWN = true;
            }
        }

    }







    _framerate.pollFramerate();

    al_get_mouse_state(&_mouseState);
    _window->getMouse(&_mouseState, _xMouse, _yMouse);

    al_wait_for_event(_eventQueue, &_event);

    al_flush_event_queue(_eventQueue);

    if (_input.getKey(ALLEGRO_KEY_ESCAPE))
        _quit = true;

    if (!_input.getKey(ALLEGRO_KEY_SPACE)) _keyFull = false;
    if (_input.getKey(ALLEGRO_KEY_SPACE) && !_keyFull)
    {
        _keyFull = true;
        _window->toggleFullScreen(-1);
        //_window->toggleFullScreen(0);
    }

    if (!_input.getKey(ALLEGRO_KEY_TAB)) _keySwitch = false;
    if (_input.getKey(ALLEGRO_KEY_TAB) && !_keySwitch)
    {
        _keySwitch = true;
        _window->switchMonitor(-1);
    }



    // simulate button pressed !
    if (_input.getKey(ALLEGRO_KEY_Z)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_UP, true);
    if (_input.getKey(ALLEGRO_KEY_S)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_DOWN, true);
    if (_input.getKey(ALLEGRO_KEY_Q)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_LEFT, true);
    if (_input.getKey(ALLEGRO_KEY_D)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_RIGHT, true);

    _input.pollJoystick();

    float speed = 1;

    if (_manPlayer.getPlayer(0)->getController()->getButton(PAD_UP))
    {
        if (hero.CAN_UP)
        {
            if (hero.AUTO_LEFT) hero.move(-speed,0,0);
            if (hero.AUTO_RIGHT) hero.move(speed,0,0);
        }

        if (!hero.AUTO_UP) hero.move( 0,-speed, 0);

        hero.D_UP = true;
    }
    else
    {
        hero.D_UP = false;
    }


    if (_manPlayer.getPlayer(0)->getController()->getButton(PAD_DOWN))
    {
        if (hero.CAN_DOWN)
        {
            if (hero.AUTO_LEFT) hero.move(-speed,0,0);
            if (hero.AUTO_RIGHT) hero.move(speed,0,0);
        }

        if (!hero.AUTO_DOWN) hero.move( 0, speed, 0);

        hero.D_DOWN = true;
    }
    else
    {
        hero.D_DOWN = false;
    }

    if (_manPlayer.getPlayer(0)->getController()->getButton(PAD_LEFT))
    {
        if (hero.CAN_LEFT)
        {
            if (hero.AUTO_UP) hero.move(0,-speed,0);
            if (hero.AUTO_DOWN) hero.move(0,speed,0);
        }

        if (!hero.AUTO_LEFT) hero.move(-speed, 0, 0);

        hero.D_LEFT = true;
    }
    else
    {
        hero.D_LEFT = false;
    }

    if (_manPlayer.getPlayer(0)->getController()->getButton(PAD_RIGHT))
    {
        if (hero.CAN_RIGHT)
        {
            if (hero.AUTO_UP) hero.move(0,-speed,0);
            if (hero.AUTO_DOWN) hero.move(0,speed,0);
        }

        if (!hero.AUTO_RIGHT) hero.move( speed, 0, 0);

        hero.D_RIGHT = true;
    }
    else
    {
        hero.D_RIGHT = false;
    }



    caseX = int(_xMouse/caseSize) * caseSize;
    caseY = int(_yMouse/caseSize) * caseSize;

    mapX = caseX/caseSize;
    mapY = caseY/caseSize;

    if (_mouseState.buttons & 1)
        level.set(mapX, mapY, 1);

    if (_mouseState.buttons & 2)
        level.set(mapX, mapY, 0);


    hero.update();
}
