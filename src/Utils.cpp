#include "Game.h"

void drawSight(float x, float y, int screenW, int screenH, ALLEGRO_COLOR color)
{
    al_draw_line(0,y+.5,
                 screenW,y+.5,
                 color,0);

    al_draw_line(x+.5,0,
                 x+.5,screenH,
                 color,0);
}

void drawRect(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color, float thickness)
{
    al_draw_rectangle(x1+.5, y1+.5, x2+.5, y2+.5, color, thickness);
}

void drawFillRect(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color)
{
    al_draw_filled_rectangle(x1+.5, y1+.5, x2+.5, y2+.5, color);
}


void drawCase(unsigned x, unsigned y, int s)
{
    al_draw_filled_rectangle(x*s+.5, y*s+.5, x*s+.5+s, y*s+.5+s, al_map_rgba(0,150,0,50));
}

void drawMap2D(Map2D &map2D, int tileSize, Game &game)
{
    for(unsigned j(0); j<map2D.h; j++)
    {
        for(unsigned i(0); i<map2D.w ; i++)
        {
            float tileX = (i * tileSize);// + .5;
            float tileY = (j * tileSize);// + .5;

            al_draw_bitmap(game.groundBmp, tileX, tileY, 0);

            if (map2D.get(i,j) == 1)
            {
                //al_draw_filled_rounded_rectangle(tileX, tileY, tileX+tileSize, tileY+tileSize, 4, 4, al_map_rgb(50,120,100));
                //al_draw_filled_rectangle(tileX, tileY, tileX+tileSize, tileY+tileSize, al_map_rgb(250,120,100));
                //al_draw_rounded_rectangle(tileX, tileY, tileX+tileSize, tileY+tileSize, 4, 4, al_map_rgb(20,50,30),0);

                al_draw_bitmap(game.blocBmp, tileX, tileY, 0);
            }

        }
    }
}

void loadMap2D(Map2D &map2D)
{
    for(unsigned i(0); i<map2D.w; i++)
    {
        map2D.set( i, 0, 1);
        map2D.set( i, map2D.h-1, 1);
    }

    for(unsigned j(0); j<map2D.h ; j++)
    {
        map2D.set( 0, j, 1);
        map2D.set( map2D.w-1, j, 1);
    }

    for (int i(0); i<map2D.w-2 ; i+=2)
    {
        for (int j(0); j<map2D.h-2 ; j+=2)
        {
            map2D.set( i, j, 1);
        }
    }

    //map2D.set( random(0,map2D.w), random(0,map2D.h), 1);

    // Debug : Show map2D !
    for(unsigned j(0); j<map2D.h; j++)
    {

        for(unsigned i(0); i<map2D.w ; i++)
        {
            if (map2D.get( i, j) == 0)
                std::cout << " ";

            if (map2D.get( i, j) == 1)
                std::cout << "X";

            //std::cout << map2D.array2D[i][j];
        }
        std::cout << "\n";
    }

}
