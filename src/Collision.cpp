#include "Collision.h"

bool inRange(int val, int mini, int maxi)
{
    return (val>mini && val<maxi);
}

bool inRect(int x, int y, Rect &rect )
{
    return (inRange(x,rect.x,rect.x+rect.w) &&
            inRange(y,rect.y,rect.y+rect.h));
}

CollidePos inRectPos(int x, int y, Rect& rect)
{
    CollidePos colPos = COL_NULL;

    int cx = rect.x + rect.w/2;
    int cy = rect.y + rect.h/2;

    if (inRect(x, y, rect))
    {
        if (y < cy) colPos = COL_DOWN;
        if (y > cy) colPos = COL_UP;
        if (x < cx) colPos = COL_RIGHT;
        if (x > cx) colPos = COL_LEFT;
    }

    return colPos;
}


bool rectHitRect(Rect &rect1, Rect &rect2)
{
    return !(rect1.x > rect2.x + rect2.w ||
             rect1.x + rect1.w < rect2.x ||
             rect1.y > rect2.y + rect2.h ||
             rect1.y + rect1.h < rect2.y);
}

CollidePos rectHitRectPos(Rect& rect1, Rect& rect2)
{
    CollidePos colPos = COL_NULL;

    int cx1 = rect1.x + rect1.w/2;
    int cy1 = rect1.y + rect1.h/2;
    int cx2 = rect2.x + rect2.w/2;
    int cy2 = rect2.y + rect2.h/2;

    if (rectHitRect(rect1, rect2))
    {
        if (cy1 < cy2) colPos = COL_DOWN;
        if (cy1 > cy2) colPos = COL_UP;
        if (cx1 < cx2) colPos = COL_RIGHT;
        if (cx1 > cx2) colPos = COL_LEFT;
    }

    return colPos;
}
