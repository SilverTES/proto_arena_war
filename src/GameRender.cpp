#include "Game.h"


void Game::render()
{
    _window->beginRender();

    al_clear_to_color(al_map_rgb(0,80,100));


    drawMap2D(level, caseSize, *this);

    //drawSight(hero.x, hero.y, _screenW, _screenH, al_map_rgba(0,80,100,10));

    //drawGrid(caseSize,caseSize, al_map_rgba(20,40,60,100), _screenW, _screenH);

    hero.draw(heroBmp);


    for (unsigned i(0); i<4; i++)
    {
//        float mapX = (p[i].x/caseSize);
//        float mapY = (p[i].y/caseSize);
//        drawCase(mapX, mapY, caseSize);
        al_draw_rectangle(p[i].x+.5, p[i].y+.5, p[i].x+.5, p[i].y+.5, al_map_rgba(255,0,0,50), 1);
    }


    //al_draw_rounded_rectangle(caseX, caseY, caseX+caseSize, caseX+caseSize, 4,4, al_map_rgb(250,250,0), 0);
    al_draw_rectangle(caseX+.5, caseY+.5, caseX+.5+caseSize, caseY+.5+caseSize, al_map_rgba(250,250,0,100), 0);

    al_draw_textf(_mainFont, al_map_rgb(0,250,120), caseX, caseY, 0, "%i,%i", int(caseX/caseSize), int(caseY/caseSize));

    drawSight(_xMouse, _yMouse, _screenW, _screenH,  al_map_rgba(200,180,100,50));
    //al_draw_bitmap(_mouseCursor, _xMouse, _yMouse, 0);

    al_draw_textf(_mainFont, al_map_rgb(205,200,200), _screenW -64, 2, 0,
                  "FPS: %i", _framerate.getFramerate());

    _window->endRender();
}

