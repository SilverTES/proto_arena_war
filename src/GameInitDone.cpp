#include "Game.h"



int Game::init()
{
    srand(time(NULL));

    _window = std::make_shared<Window>();

    _screenW = 640-16;
    _screenH = 360-24;

    al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);

    _window->init("-- Proto Arena War --", _screenW, _screenH, 2, 0, false);
    _eventQueue = al_create_event_queue();
    _framerate.setFramerate(_eventQueue, 60);

    //al_get_standard_path(ALLEGRO_EXENAME_PATH);

    //_font = al_load_font("data/gohufont-11.tff", 11, 0);
    _mainFont = al_load_font("data/Kyrou.ttf", 8, 0);
    if (!_mainFont)
        return log(1,"- Unable to load font !\n");



    heroBmp = al_load_bitmap("data/hero.png");
    blocBmp = al_load_bitmap("data/bloc.png");
    groundBmp = al_load_bitmap("data/ground.png");

    al_hide_mouse_cursor(_window->display());

    level.init(_screenW/caseSize,_screenH/caseSize);

    loadMap2D(level);


    _manPlayer.add(new Player("Mugen"));

    _manPlayer.getPlayer(0)->getController()->addButton(PAD_UP,    0, 0, 2, 7, 1, -1);
    _manPlayer.getPlayer(0)->getController()->addButton(PAD_DOWN,  0, 0, 2, 7, -1, -1);
    _manPlayer.getPlayer(0)->getController()->addButton(PAD_LEFT,  0, 0, 2, 6, -1, -1);
    _manPlayer.getPlayer(0)->getController()->addButton(PAD_RIGHT, 0, 0, 2, 6, 1, -1);

    hero.init(caseSize*10-caseSize/2,caseSize*6-caseSize/2,0,caseSize,caseSize);


    std::cout << "size of level : "<< sizeof(level) << "\n";

    return log(0,"- init Game OK !\n");
}

int Game::done()
{
    al_destroy_bitmap(heroBmp);
    al_destroy_bitmap(groundBmp);
    al_destroy_bitmap(blocBmp);
    al_destroy_font(_mainFont);
    if (_eventQueue) al_destroy_event_queue(_eventQueue);
    _window->done();

    return log(0,"- done Game OK !\n");
}
