#ifndef GAME_H
#define GAME_H

#include <MugenEngine.h>
#include "Collision.h"

class Game;



struct vec2
{
    float x;
    float y;
};

struct Hero
{
    // contrainte mouvement
    bool MOVE_UP    = true;
    bool MOVE_DOWN  = true;
    bool MOVE_LEFT  = true;
    bool MOVE_RIGHT = true;

    // Possiblité de mouvements annexe : style Bomverman
    // exemple : quand on appuie dans une direction gauche et le perso peut monter ou descendre selon la case à coté (dessus ou dessous) est passable !
    bool CAN_UP    = false;
    bool CAN_DOWN  = false;
    bool CAN_LEFT  = false;
    bool CAN_RIGHT = false;

    // Mouvement de décalage automatique si passage sur le côté libre !
    bool AUTO_UP    = false;
    bool AUTO_DOWN  = false;
    bool AUTO_LEFT  = false;
    bool AUTO_RIGHT = false;

    bool D_UP    = false;
    bool D_DOWN  = false;
    bool D_LEFT  = false;
    bool D_RIGHT = false;

    float x;
    float y;
    float z;
    float w;
    float h;
    float rw;
    float rh;

    unsigned mapX;
    unsigned mapY;

    void update()
    {
        MOVE_UP    = true;
        MOVE_DOWN  = true;
        MOVE_LEFT  = true;
        MOVE_RIGHT = true;

        CAN_UP    = false;
        CAN_DOWN  = false;
        CAN_LEFT  = false;
        CAN_RIGHT = false;

        AUTO_UP    = false;
        AUTO_DOWN  = false;
        AUTO_LEFT  = false;
        AUTO_RIGHT = false;

        mapX = int(x/w);
        mapY = int(y/h);
    }

    void init(float x, float y, float z, float w, float h)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->w = w;
        this->h = h;
        this->rw = w/2;
        this->rh = h/2;
    }

    void move(float vx, float vy, float vz)
    {
        if (MOVE_UP && vy<0)    y += vy;
        if (MOVE_DOWN && vy>0)  y += vy;
        if (MOVE_LEFT && vx<0)  x += vx;
        if (MOVE_RIGHT && vx>0) x += vx;

    }
    void draw(ALLEGRO_BITMAP * bitmap)
    {
        //drawFillRect(x-rw+2, y-rh+2, x+rw-2, y+rh-2, al_map_rgb(250,120,100));
        //drawRect(mapX*w , mapY*h, mapX*w+w, mapY*h+h, al_map_rgba(50,220,100,50),0);

        al_draw_bitmap(bitmap, x-rw,y-rh,0);

        //al_draw_rounded_rectangle(x, y, x+w, y+h, 4, 4, al_map_rgb(250,20,10),0);
    }
};

struct Map2D
{
    unsigned w;
    unsigned h;

    std::vector<std::vector<int>> array2D;

    void init(unsigned width, unsigned height)
    {
        w = width;
        h = height;

        array2D.resize(width);
        for (unsigned i = 0; i < width; ++i)
            array2D[i].resize(height);
    }

    int get(unsigned x, unsigned y)
    {
        int value;

        if (x>=0 && x<w && y>=0 && y<h)
            return array2D[x][y];
        else
            return 0;

        return value;
    }
    void set(unsigned x, unsigned y, int value)
    {
        if (x>=0 && x<w && y>=0 && y<h)
            array2D[x][y] = value;
    }
};

void drawSight(float x, float y, int screenW, int screenH, ALLEGRO_COLOR color);

void drawRect(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color, float thickness);
void drawFillRect(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color);
void drawCase(unsigned x, unsigned y, int s);
void drawMap2D(Map2D &map2D, int tileSize, Game &game);
void loadMap2D(Map2D &map2D);

class Game : public MugenEngine
{
    public:
        int init() override;
        int done() override;
        void update() override;
        void render() override;

        float caseSize = 16;
        float caseX = 0;
        float caseY = 0;
        float mapX = 0;
        float mapY = 0;

        vec2 p[4] = {0};
        Map2D level;
        Hero hero;

        ALLEGRO_BITMAP * heroBmp = NULL;
        ALLEGRO_BITMAP * blocBmp = NULL;
        ALLEGRO_BITMAP * groundBmp = NULL;


    protected:

    private:
        std::shared_ptr<Window> _window;
        Controller _input;
        Framerate _framerate;
        PlayerManager _manPlayer;

        ALLEGRO_MOUSE_STATE _mouseState;

        // Events
        ALLEGRO_EVENT_QUEUE* _eventQueue = NULL;
        ALLEGRO_EVENT _event;

        ALLEGRO_FONT * _mainFont = NULL;

        int _screenW;
        int _screenH;

        bool _keyFull;
        bool _keySwitch;

        // Mouse
        float _xMouse;
        float _yMouse;
};

#endif // GAME_H
