#ifndef COLLISION_H_INCLUDED
#define COLLISION_H_INCLUDED

struct Rect
{
    int x;
    int y;
    int w;
    int h;
};

enum CollidePos
{
    COL_NULL = 0,
    COL_UP,
    COL_DOWN,
    COL_LEFT,
    COL_RIGHT
};

bool inRange(int val, int mini, int maxi);

bool inRect(int x, int y, Rect &rect);
CollidePos inRectPos(int x, int y, Rect &rect);

bool rectHitRect(Rect &rect1, Rect &rect2);
CollidePos rectHitRectPos(Rect &rect1, Rect &rect2, bool hit);

#endif // COLLISION_H_INCLUDED
