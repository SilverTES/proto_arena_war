//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#ifndef MISC_H_INCLUDED
#define MISC_H_INCLUDED

#include "MugenEngine.h"

template <class E, class M>
static E log (E error, M msg);

int random(int beginRange, int endRange);

float pourcent(float maxValue, float value);
float proportion(float maxValue, float value, float range);

void showVideoAdapters();
void drawGrid(int sizeX, int sizeY, ALLEGRO_COLOR color, int screenW, int screenH);

#include "Misc.inl"


#endif // MISC_H_INCLUDED
