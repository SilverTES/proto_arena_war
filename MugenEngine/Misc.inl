//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#include <iostream>

template <class E, class M>
E log (E error, M msg)
{
    std::cout << msg;
    return error;
}
