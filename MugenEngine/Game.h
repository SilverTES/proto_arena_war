//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#ifndef GAME_H
#define GAME_H

#include "MugenEngine.h"

// test class
class EntityPlayer
{
public:
    EntityPlayer(int id, std::string name, float x, float y):
        _id(id),
        _name(name),
        _x(x),
        _y(y)
    {

    }
    virtual ~EntityPlayer()
    {

    }

    void update()
    {
        _up = false;
        _down = false;
        _left = false;
        _right = false;
    }
    void render(ALLEGRO_FONT * font)
    {
        al_draw_filled_ellipse(_x, _y, 48,24,al_map_rgba(40,40,20,150));
        al_draw_ellipse(_x, _y, 48,24,al_map_rgb(100,210,20),1);

        al_draw_textf(font, al_map_rgb(200,150,80), _x, _y-8, -1, "%i : %s",
                      _id, (_name).c_str());
        //al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

        al_draw_filled_circle(_x,_y-24, 4, _up ? al_map_rgb(255,0,0) : al_map_rgb(0,0,0));
        al_draw_filled_circle(_x,_y+24, 4, _down ? al_map_rgb(255,0,0) : al_map_rgb(0,0,0));
        al_draw_filled_circle(_x-48,_y, 4, _left ? al_map_rgb(255,0,0) : al_map_rgb(0,0,0));
        al_draw_filled_circle(_x+48,_y, 4, _right ? al_map_rgb(255,0,0) : al_map_rgb(0,0,0));
    }
    void setPos(float x, float y)
    {
        _x = x;
        _y = y;
    }

    int _id;
    std::string _name;
    float _x;
    float _y;
    float _z;

    bool _up = false;
    bool _down = false;
    bool _left = false;
    bool _right = false;


protected:
private:
};



class Graph
{
public:
    Graph(const char* name, float x, float y, float width, float height, float zoomX, ALLEGRO_COLOR color):
        _name(name),
        _x(x),
        _y(y),
        _width(width),
        _height(height),
        _zoomX(zoomX),
        _color(color)
    {

    }
    virtual ~Graph()
    {

    }
    void set(float x, float y, float width, float height)
    {
        _x = x;
        _y = y;
        _width = width;
        _height = height;

    }
    void update()
    {

    }
    void render(std::shared_ptr<Window> window)
    {
        al_draw_filled_rectangle(_x, _y-2, _x+_width, _y+_height, al_map_rgba(10,20,20,150));

        float lastValue(0);
        float time(0);

        for (float const & it: _deqValue)
        {
            if (time > 0)
                al_draw_line(_x+(time*_zoomX), _y+_height-lastValue,
                             _x+((time+1)*_zoomX),_y+_height-it,
                             _color,0);

            lastValue = it;
            time++;

            //al_draw_line(_x+(time*_zoomX), _y+_height,_x+(time*_zoomX),_y+_height-it, al_map_rgba(200,150,0,25),0);

        }
        //al_draw_textf(window->font(), _color, _x+2, _y+_height-_deqValue[0], 0, "%s", _name);

        al_draw_rectangle(_x+.5, _y+.5-2, _x+.5+_width, _y+.5+_height+1, al_map_rgba(10,10,0,250),0);
    }

    void addValue(float maxValue, float value)
    {

        if ( (_deqValue.size()*_zoomX) >= _width)
            _deqValue.pop_back();

        _deqValue.push_front(proportion(maxValue, value,_height));
    }
    const char* _name;
    float _x;
    float _y;
    float _width;
    float _height;
    float _zoomX;
    ALLEGRO_COLOR _color;

protected:

private:
    std::deque<float> _deqValue;
};



class Game : public MugenEngine
{
    public:
        int init() override;
        int done() override;
        void update() override;
        void render() override;

        static void *mouseThread(ALLEGRO_THREAD *thr, void *arg);

    protected:
        ALLEGRO_BITMAP * scaledBitmap (ALLEGRO_BITMAP * srcBitmap, int scale);
        void scaleMouseCursor(int scale=1);
        void runAssignButton(int player, int id);

    private:
        const char * _name;
        bool _redraw;

        ALLEGRO_MOUSE_STATE _mouseState;

        // Events
        ALLEGRO_EVENT_QUEUE* _eventQueue;
        ALLEGRO_EVENT _event;

        ALLEGRO_FONT *_font = NULL;

        Framerate _framerate;
        std::shared_ptr<Window> _window;

        Data _data;
        json _config;

        PlayerManager _manPlayer;
        // pour tester les controlleurs !
        std::vector<EntityPlayer> _vecEntityPlayer;

        std::vector<Graph> _vecGraph;

        ALLEGRO_FONT * _mainFont = NULL;
        ALLEGRO_BITMAP * _mouseCursor = NULL;
        ALLEGRO_BITMAP * _gamepadSNES = NULL;
        ALLEGRO_BITMAP * _background = NULL;

        Controller _input;

        bool _vsync = false;
        bool _isFullScreen = false;
        bool _turbo;
        int _screenW;
        int _screenH;
        int _scaleWin;
        int _scaleFull;

        bool _keyFull;
        bool _keySwitch;

        // Mouse
        float _xMouse;
        float _yMouse;


};

#endif // GAME_H
